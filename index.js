const express = require("express");
// Import mongoose package
const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://Baggam_Rakshan_Tej:x3dOSxedvkf9xy91@cluster0.1i9omqc.mongodb.net/b242-s35-activity?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// Connection to the database
let db = mongoose.connection;

// If a connection error occured, output will be in the console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful
db.once("open", () => console.log("We're connected to the cloud database"));

// Mongoose Schemas
// Creates a new task schema
const taskSchema = new mongoose.Schema({
	// task field and task status
	username : String,
	password : String,
});

// Task model
const User = mongoose.model("User", taskSchema);

// if the task is already present, it shows an error..otherwise it is added to the database


app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res) => {
	User.findOne({username : req.body.username}, (err, result) => {
		if(result !== null && result.username === req.body.username && result.body.password === req.body.password){
			return res.send("User already exists");
		}
		else{
			let newUser = new User({
				username : req.body.userName,
				password : req.body.password
			})

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New User registered");
				}
			})
		}
	})
})

// GET Request to retreive all the tasks

app.get("/Users", (req, res) => {
	User.find({}, (err, result) => {
		//similar to find in mongodb
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	}) 
})


app.listen(port, () => console.log(`Server is running at ${port}`));